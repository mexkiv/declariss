(function(){
	'use strict';

	var app = angular.module('Myfilters', []);

	app.filter('codFormat', function () {
        return function (n, len) {
            var num = parseInt(n, 10);
            len = parseInt(len, 10);
            if (isNaN(num) || isNaN(len)) {
                return n;
            }
            num = ''+num;
            while (num.length < len) {
                num = '0'+num;
            }
            return num;
        };
    });

    app.filter('dateFormat', function () {
        return function (val) {
            
            var dateSplit = val.split(' ');
            var date = new Date(dateSplit[0]);

            date = ('0' + (date.getDate() + 1)).slice(-2) + '/' + ('0' + (date.getMonth() + 1)).slice(-2) + '/' + date.getFullYear();

            return date;
        };
    });

    app.filter('currencyFormat', function () {
        return function (val) {
            if(val) {
                if(val.search('.') != -1) {
                    val = val.replace('.',',');
                }
            }
            return val;
        };
    });

}());