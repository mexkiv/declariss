<?php

namespace NotaryUses;

use Respect\Validation\Validator as v;

require_once APP . '/models/notaryuse.php';

// retrieve

function retrieve($app, $id = 0) {
	$limit = ($app->request()->params('limit')) ? $app->request()->params('limit') : 30;
	$offset = ($app->request()->params('offset')) ? $app->request()->params('offset') : 0;

	if($id)
		$uses = \NotaryUse::find($id);
	else
		$uses = \NotaryUse::take($limit)->offset($offset)->orderBy('id_serventia', 'desc')->get();

    if($uses)
    	\utils::json($app, $uses->toJson());
    else
    	\utils::json($app, array('message' => 'ID ' . $id . ' not exists'), true);
}

// create

function create($app) {
	$body = $app->request->getBody();
	$body = json_decode($body);

	if(is_object($body)) {
		$validation = v::attribute('nome', v::string()->length(1,45));

		if($validation->validate($body)) {
			$notaryUse = new \NotaryUse();
			$notaryUse->nome = $body->nome;

			$id = $notaryUse->save();

			if($id) {
				\utils::json($app, $notaryUse->toJson());
				return;
			}
		}
	}

	$app->response->setStatus(400);
}

// update

function update($app, $id) {
	$body = $app->request->getBody();
	$body = json_decode($body);

	if(is_object($body)) {
		$validation = v::attribute('nome', v::string()->length(1,45));

		if($validation->validate($body)) {
			$notaryUse = \NotaryUse::find($id);
			$notaryUse->nome = $body->nome;

			$id = $notaryUse->save();

			if($id) {
				\utils::json($app, $notaryUse->toJson());
				return;
			}
		}
	}

	$app->response->setStatus(400);
}

// remove

function remove($app, $id) {
	$notaryUse = \NotaryUse::find($id);

	if($notaryUse) {
		$notaryUse->delete();
		\utils::json($app, array('message' => 'ID ' . $id . ' removed'), true);
	} else {
		\utils::json($app, array('message' => 'ID ' . $id . ' not exists'), true);
	}
}
