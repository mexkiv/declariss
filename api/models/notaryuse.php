<?php

class NotaryUse extends \Illuminate\Database\Eloquent\Model {
	protected $table = 'cartorio_serventia';
	protected $primaryKey = 'id_serventia';
    public $timestamps = false;
}
