(function(){    

    'use strict';

    MetronicApp.factory('Townhalls', ['$http', 'Utils', function($http, Utils) {
        
        var townhall = {};
        var api = '../api/townhalls';

        townhall.getAll =  function(id, callback) {
            return $http.get(api).success(callback);
        }

        townhall.getById =  function(id, callback) {
            return $http.get(api + '/' + id).success(callback);
        }

        townhall.create = function(data, callback) {
        	return $http.post(api, data).success(callback);
        }

        townhall.update = function(id, data, callback) {
        	return $http.put(api + '/' + id, data).success(callback);
        }

        townhall.delete = function(id, callback1, callback2) {
        	return $http.delete(api + '/' + id).success(callback1).error(callback2);
        }


        return townhall;

    }]);

}());