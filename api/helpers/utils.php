<?php

class utils {

	static function json($app, $data = null, $encode = false) {

		$app->response()->header('Content-Type', 'application/json');
		
	    if(!is_null($data) && $encode)
	    	echo json_encode($data);
	    else 
	    	echo $data;

	    $app->response->setStatus(200);

	}

	static function debug($var) {

	    print_r($var);
	    die();

	}

}