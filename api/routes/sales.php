<?php

require_once 'controllers/sales.php';
require_once 'controllers/auth.php';

//GET Route
$app->get('/sales', function () use ($app) {
	\Auth\isAuthenticate();
	\Sales\retrieve($app);
});

//GET Route
$app->get('/sales/:id', function ($id) use ($app) {
	\Auth\isAuthenticate();
	\Sales\retrieve($app, $id);
});

//POST Route
$app->post('/sales', function () use ($app) {
	\Auth\isAuthenticate();
	\Sales\create($app);
});

//DELETE Route
$app->delete('/sales/:id/', function ($id) use ($app) {
	\Auth\isAuthenticate();
	\Sales\remove($app, $id);
});