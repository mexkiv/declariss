/***
AngularJS App  / Routes
***/

/* Metronic App */
var MetronicApp = angular.module("MetronicApp", []); 

MetronicApp.config(['$controllerProvider', function($controllerProvider) {
  $controllerProvider.allowGlobals();
}]);
