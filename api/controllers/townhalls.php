<?php

namespace TownHalls;

use Respect\Validation\Validator as v;

require_once APP . '/models/townhall.php';
require_once APP . '/models/user.php';

//Retrieve
function retrieve($app, $id = 0){

	$limit = ($app->request()->params('limit')) ? $app->request()->params('limit') : 30;
	$offset = ($app->request()->params('offset')) ? $app->request()->params('offset') : 0;

	if($id)
		$townhalls = \TownHall::find($id);
	else
		$townhalls = \TownHall::take($limit)->offset($offset)->orderBy('id_prefeitura', 'desc')->get();

    if($townhalls)
    	\utils::json($app, $townhalls->toJson());
    else 
    	\utils::json($app, array('message' => 'ID ' . $id . ' not exists'), true);

}

//Create
function create($app){

	$body = $app->request->getBody();
	$body = json_decode($body);

	if(is_object($body)) {

		$townhallValidator = v::attribute('cnpj', v::string()->length(18,18))
						->attribute('nome', v::string()->length(5,45))
						->attribute('cidade', v::string()->length(5,45))
						->attribute('uf', v::string()->length(6,45));

		if($townhallValidator->validate($body)) {

			$townhall = new \TownHall();
			$townhall->cnpj = $body->cnpj;
			$townhall->nome = $body->nome;
	    	$townhall->cidade = $body->cidade;
	    	$townhall->uf = $body->uf;
			$id = $townhall->save();

			if($id) {
				\utils::json($app, $townhall->toJson());
				return;
			}

		}

	}
	
	$app->response->setStatus(400);

}

//Update
function update($app, $id){

	$body = $app->request->getBody();
	$body = json_decode($body);

	if(is_object($body)) {

		$userValidator = v::attribute('cnpj', v::string()->length(18,18))
						->attribute('nome', v::string()->length(5,45))
						->attribute('cidade', v::string()->length(5,45))
						->attribute('uf', v::string()->length(6,45));

		if($userValidator->validate($body)) {

			$townhall = \TownHall::find($id);
			$townhall->cnpj = $body->cnpj;
			$townhall->nome = $body->nome;
	    	$townhall->cidade = $body->cidade;
	    	$townhall->uf = $body->uf;
			$id = $townhall->save();

			if($id) {
				\utils::json($app, $townhall->toJson());
				return;
			}

		}

	}
	
	$app->response->setStatus(400);

}

//Remove
function remove($app, $id){

	$townhalls = \UserHasTowHall::where('id_prefeitura', '=', $id)->get();

	if($townhalls->count() == 0){
		$townhall = \TownHall::find($id);
		if($townhall) {
			$townhall->delete();
			\utils::json($app, array('message' => 'ID ' . $id . ' removed'), true);
		} else {
			\utils::json($app, array('message' => 'ID ' . $id . ' not exists'), true);
		}
	} else {
		$app->response->setStatus(400);
	}

}