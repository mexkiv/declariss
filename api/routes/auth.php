<?php

require_once 'controllers/auth.php';

//GET Route
$app->get('/auth', function () use ($app) {
	\Auth\isLogged($app);
});

//GET Route
$app->get('/logout', function () use ($app) {
	\Auth\doLogout($app);
});


//POST Route
$app->post('/auth', function () use ($app) {
	\Auth\doLogin($app);
});
