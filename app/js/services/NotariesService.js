(function(){    

    'use strict';

    MetronicApp.factory('Notaries', ['$http', 'Utils', function($http, Utils) {
        
        var notary = {};
        var api = '../api/notarys';

        notary.getAll =  function(id, callback) {
            return $http.get(api).success(callback);
        }

        notary.getById =  function(id, callback) {
            return $http.get(api + '/' + id).success(callback);
        }

        notary.getUses = function(id, callback) {
            return $http.get(api + '/notaryuses/' + id).success(callback);
        }

        notary.getServices = function(id, callback) {
            return $http.get(api + '/notaryservices/' + id).success(callback);
        }

        notary.create = function(data, callback) {
            return $http.post(api, data).success(callback);
        }

        notary.update = function(id, data, callback) {
        	return $http.put(api + '/' + id, data).success(callback);
        }

        notary.delete = function(id, callback1, callback2) {
            return $http.delete(api + '/' + id).success(callback1).error(callback2);
        }

        return notary;
    }]);
}());
