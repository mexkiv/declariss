(function(){   

	'use strict';

	MetronicApp.controller('UsersList', ['$rootScope', '$scope', '$location', 'Users', function($rootScope, $scope, $location, Users){

	    $scope.$on('$viewContentLoaded', function() {   
	    	Metronic.initAjax();
	    });

	    Users.getAll(null, function(data){
	    	$scope.users = data;
	    });

	    $scope.save = function(user){
	    	Users.create(user, function(data){
	    		$location.path('/users.html');
	    	})
	    }

	    $scope.modal = function(user) {
	    	$scope.userm = user;
	    }

	    $scope.remove = function(user) {
	    	Users.delete(user.id_usuario, function(){
	    		var index = $scope.users.indexOf(user);
				$scope.users.splice(index, 1);
	    	})
	    }


	}]);

	MetronicApp.controller('UserCreate', ['$rootScope', '$scope', '$location', 'Users', 'Townhalls', function($rootScope, $scope, $location, Users, Townhalls){

	    $scope.$on('$viewContentLoaded', function() {   
	    	Metronic.initAjax();
	    });

	    $scope.townchecks = Array();

    	Townhalls.getAll(null, function(towns){
	    	$scope.townhalls = towns;
	    });

    	$scope.addItem = function(townhall){
	    	var index = $scope.townchecks.indexOf(townhall);
	    	if(index == -1){
	    		$scope.townchecks.push(townhall);
	    	}
	    }

	    $scope.removeItem = function(townhall){
	    	var index = $scope.townchecks.indexOf(townhall);
	    	$scope.townchecks.splice(index, 1);
	    }

	    $scope.save = function(user, townhalls){
	    	user.prefeituras = townhalls;
	    	Users.create(user, function(data){
	    		$location.path('/users.html');
	    	})
	    }

	}]);

	MetronicApp.controller('UserUpdate', ['$rootScope', '$scope', '$location', '$stateParams', 'Users', 'Townhalls',function($rootScope, $scope, $location, $stateParams, Users, Townhalls){

	    $scope.$on('$viewContentLoaded', function() {   
	    	Metronic.initAjax();
	    });

	    $scope.townchecks = [];
		Users.getById($stateParams.id, function(data){

			$scope.townchecks = Array();
			delete data.senha;
	    	$scope.user = data;

	    	Townhalls.getAll(null, function(towns){
		    	$scope.townhalls = towns;
		    });

	    	Users.getTownhalls(data.id_usuario, function(towns){
	    		$scope.townchecks = towns;
	    	});

	    });
		
	    $scope.addItem = function(townhall){
	    	
	    	var index = $scope.townchecks.indexOf(townhall);
	    	if(index == -1) {
	    		$scope.townchecks.push(townhall);
	    		
	    	}
	    }

	    $scope.removeItem = function(townhall){
	    	var index = $scope.townchecks.indexOf(townhall);
	    	$scope.townchecks.splice(index, 1);
	    }

	    $scope.save = function(user, townhalls){
	    	user.prefeituras = townhalls;
	    	Users.update(user.id_usuario, user, function(data){
	    		$location.path('/users.html');
	    	})
	    }

	}]);

}());
