(function(){   

	'use strict';

	MetronicApp.controller('LoginController', ['$scope', '$window', 'Auth', function($scope, $window, Auth){

		$scope.error = false;

	    $scope.login = function(user){
	    	Auth.doLogin(user, function(data){
	    		$scope.error = false;
	    		$window.location = 'app/index.html'
	    	},function(err, cod){
	    		$scope.error = true;
	    	});
	    }


	}]);

	MetronicApp.controller('LogoutController', ['$scope', '$window', 'Auth', function($scope, $window, Auth){

		Auth.doLogout(function(data){
			$window.location = '../index.html';
		});

	}]);

	

}());
