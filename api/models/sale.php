<?php

class Sale extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'venda';
    protected $primaryKey = 'id_venda';
    public $timestamps = false;

}