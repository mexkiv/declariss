(function(){   

	'use strict';

	MetronicApp.controller('SalesList', ['$rootScope', '$scope', '$location', 'Sales',function($rootScope, $scope, $location, Sales){

	    $scope.$on('$viewContentLoaded', function() {   
	    	Metronic.initAjax();
	    });

	    Sales.getAll(null, function(data){
	    	$scope.sales = data;
	    });

	    $scope.calcTotal = function(item) {
	    	item.total = item.valor * item.quantidade;
	    }

	    $scope.save = function(service){
	    	Sales.create(service, function(data){
	    		$location.path('/sales.html');
	    	})
	    }

	    $scope.modal = function(service) {
	    	$scope.salem = service;
	    }

	    $scope.remove = function(service) {
	    	Sales.delete(service.id_venda, function(){
	    		var index = $scope.sales.indexOf(service);
				$scope.sales.splice(index, 1);
	    	})
	    }


	}]);

	
	MetronicApp.controller('SalesCreate', ['$rootScope', '$scope', '$location', 'Services', 'Sales',function($rootScope, $scope, $location, Services, Sales){

	    $scope.$on('$viewContentLoaded', function() {   
	    	Metronic.initAjax();
	    });

	    Services.getAll(null, function(data){
	    	$scope.services = data;
	    });

	    $scope.items = [];
	    $scope.addItem = function(item) {
	    	var index = $scope.items.indexOf(item);
	    	if(index == -1){
	    		item.quantidade = 1;
	    		item.total = item.valor;
	    		$scope.items.push(item);
	    	}
	    }

	    $scope.quant = function(item, signal) {
	    	if(signal == '+'){
	    		item.quantidade = item.quantidade + 1;
	    		item.total = item.valor * item.quantidade;
	    	}
	    	if(signal == '-' && item.quantidade > 1) {
	    		item.quantidade = item.quantidade - 1;
	    		item.total = item.valor * item.quantidade;
	    	}
	    }

	    $scope.remove = function(item) {
	    	var index = $scope.items.indexOf(item);
			$scope.items.splice(index, 1);
	    }

	    $scope.save = function(items){
	    	Sales.create(items, function(data){
	    		$location.path('/sales.html');
	    	})
	    }

	}]);



}());
