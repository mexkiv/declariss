(function(){    

    'use strict';

    MetronicApp.factory('NotaryUses', ['$http', 'Utils', function($http, Utils) {
        
        var notaryuse = {};
        var api = '../api/notaryuses';

        notaryuse.getAll =  function(id, callback) {
            return $http.get(api).success(callback);
        }

        notaryuse.getById =  function(id, callback) {
            return $http.get(api + '/' + id).success(callback);
        }

        notaryuse.create = function(data, callback) {
        	return $http.post(api, data).success(callback);
        }

        notaryuse.update = function(id, data, callback) {
        	return $http.put(api + '/' + id, data).success(callback);
        }

        notaryuse.delete = function(id, callback) {
            return $http.delete(api + '/' + id).success(callback);
        }

        return notaryuse;
    }]);
}());
