(function(){

	'use strict';

	MetronicApp.controller('NotaryUsesList', ['$rootScope', '$scope', '$location', 'NotaryUses', function($rootScope, $scope, $location, NotaryUses) {

	    $scope.$on('$viewContentLoaded', function() {   
	    	Metronic.initAjax();
	    });

	    NotaryUses.getAll(null, function(data){
	    	$scope.notaryuses = data;
	   	});

	    $scope.save = function(notaryuse){
	    	NotaryUses.create(notaryuse, function(data){
	    		$location.path('/notaryuses.html');
	    	});
	    }

	    $scope.modal = function(notaryuse_modal) {
	    	$scope.notaryuse_modal = notaryuse_modal;
	    }

	    $scope.remove = function(notaryuse) {
	    	NotaryUses.delete(notaryuse.id_serventia, function() {
	    		var index = $scope.notaryuses.indexOf(notaryuse);
	    		$scope.notaryuses.splice(index, 1);
	    	});
	    }
	}]);

	MetronicApp.controller('NotaryUseCreate', ['$rootScope', '$scope', '$location', 'NotaryUses', function($rootScope, $scope, $location, NotaryUses) {

	    $scope.$on('$viewContentLoaded', function() { 
	    	Metronic.initAjax();
	    });

		$scope.title = 'Nova serventia';
	    $scope.action = 'Cadastrar';

	    $scope.save = function(notaryuse) {
	    	NotaryUses.create(notaryuse, function(data) {
	    		$location.path('/notaryuses.html');
	    	});
	    }
	}]);

	MetronicApp.controller('NotaryUseUpdate', ['$rootScope', '$scope', '$location', '$stateParams', 'NotaryUses', function($rootScope, $scope, $location, $stateParams, NotaryUses) {

		$scope.$on('$viewContentLoaded', function() {
			Metronic.initAjax();
	    });

		NotaryUses.getById($stateParams.id, function(data) {
			$scope.notaryuse = data;
	    });

	    $scope.title = 'Editar serventia';
		$scope.action = 'Editar';

	    $scope.save = function(notaryuse) {
	    	NotaryUses.update(notaryuse.id_serventia, notaryuse, function(data) {
	    		$location.path('/notaryuses.html');
	    	});
	    }
	}]);

}());
