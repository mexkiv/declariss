<?php

namespace Services;

use Respect\Validation\Validator as v;

require_once APP . '/models/service.php';

//Retrieve
function retrieve($app, $id = 0){

	$limit = ($app->request()->params('limit')) ? $app->request()->params('limit') : 30;
	$offset = ($app->request()->params('offset')) ? $app->request()->params('offset') : 0;

	if($id)
		$services = \Service::find($id);
	else
		$services = \Service::take($limit)->offset($offset)->orderBy('id_servico', 'desc')->get();

    if($services)
    	\utils::json($app, $services->toJson());
    else 
    	\utils::json($app, array('message' => 'ID ' . $id . ' not exists'), true);

}

//Create
function create($app){

	$body = $app->request->getBody();
	$body = json_decode($body);

	if(is_object($body)) {

		$serviceValidation = v::attribute('ato', v::string()->length(1,45))
						->attribute('valor', v::float())
						->attribute('tipo', v::string()->length(1,45));

		if($serviceValidation->validate($body)) {

			$service = new \Service();
			$service->ato = $body->ato;
			$service->valor = $body->valor;
	    	$service->tipo = $body->tipo;
			$id = $service->save();

			if($id) {
				\utils::json($app, $service->toJson());
				return;
			}

		}

	}
	
	$app->response->setStatus(400);

}

//Update
function update($app, $id){

	$body = $app->request->getBody();
	$body = json_decode($body);

	if(is_object($body)) {

		$serviceValidation = v::attribute('ato', v::string()->length(1,45))
						->attribute('valor', v::float())
						->attribute('tipo', v::string()->length(1,45));

		if($serviceValidation->validate($body)) {

			$service = \Service::find($id);
			$service->ato = $body->ato;
			$service->valor = $body->valor;
	    	$service->tipo = $body->tipo;
			$id = $service->save();

			if($id) {
				\utils::json($app, $service->toJson());
				return;
			}

		}

	}
	
	$app->response->setStatus(400);

}

//Remove
function remove($app, $id){

	$service = \Service::find($id);

	if($service) {
		$service->delete();
		\utils::json($app, array('message' => 'ID ' . $id . ' removed'), true);
	} else {
		\utils::json($app, array('message' => 'ID ' . $id . ' not exists'), true);
	}


}