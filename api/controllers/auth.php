<?php

namespace Auth;

use Respect\Validation\Validator as v;

require_once APP . '/models/user.php';

//Is Logged
function isLogged($app){

	session_start();

	if(array_key_exists('user', $_SESSION)){

		$user = $_SESSION['user'];
		$user = unserialize($user);

		if(($_SERVER['REMOTE_ADDR'] == $user['ip']) && ($_SERVER['HTTP_USER_AGENT'] == $user['navegador'])) {
			\utils::json($app, $user, true);
			return;
		}

	}
	
	$app->response->setStatus(401);
	return;

}

//Is Authenticate
function isAuthenticate(){

	session_start();

	if(array_key_exists('user', $_SESSION)){

		$user = $_SESSION['user'];
		$user = unserialize($user);

		if(($_SERVER['REMOTE_ADDR'] == $user['ip']) && ($_SERVER['HTTP_USER_AGENT'] == $user['navegador'])) {
			return;
		}

	}
	
	die('Not Authenticated!');

}

//Do logout
function doLogout($app){

	session_start();
	session_destroy();
	$app->response->setStatus(200);

}

//Do Login
function doLogin($app){

	$body = $app->request->getBody();
	$body = json_decode($body);

	if(is_object($body)) {

		$emailValidation = v::attribute('email', v::string()->length(1,45))
		                    ->attribute('senha', v::string()->length(1,45));

		if($emailValidation->validate($body)) {

			$user = \User::where('email', '=', $body->email)->limit(1)->get();
			if($user->count()) {

				$user = $user->toArray()[0];

				if(crypt($body->senha, $user['senha']) == $user['senha']){
					
					$user_session['id_usuario'] = $user['id_usuario'];
					$user_session['nome'] = $user['nome'];
					$user_session['email'] = $user['email'];
					$user_session['ip'] = $_SERVER['REMOTE_ADDR'];
					$user_session['navegador'] = $_SERVER['HTTP_USER_AGENT'];

					session_start();
					$_SESSION['user'] = serialize($user_session);
					\utils::json($app, $user_session, true);
					return;

				}

			}

		}

	} 

	$app->response->setStatus(401);
	
}