/***
AngularJS App  / Routes
***/

/* Metronic App */
var MetronicApp = angular.module("MetronicApp", [
    "Myfilters",
    "ui.mask",
    "ui.utils.masks",
    "ui.router", 
    "ui.bootstrap", 
    "oc.lazyLoad",  
    "ngSanitize"
]); 

/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
MetronicApp.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        cssFilesInsertBefore: 'ng_load_plugins_before'
    });
}]);


//AngularJS v1.3.x workaround for old style controller declarition in HTML
MetronicApp.config(['$controllerProvider', function($controllerProvider) {
  $controllerProvider.allowGlobals();
}]);


/* Setup global settings */
MetronicApp.factory('settings', ['$rootScope', function($rootScope) {
    // supported languages
    var settings = {
        layout: {
            pageSidebarClosed: false, 
            pageAutoScrollOnLoad: 1000 
        },
        layoutImgPath: Metronic.getAssetsPath() + 'admin/layout/img/',
        layoutCssPath: Metronic.getAssetsPath() + 'admin/layout/css/'
    };

    $rootScope.settings = settings;

    return settings;
}]);


/* Setup App Main Controller */
MetronicApp.controller('AppController', ['$scope', '$rootScope', function($scope, $rootScope) {
    $scope.$on('$viewContentLoaded', function() {
        Metronic.initComponents(); 
        //Layout.init();
    });
}]);

/***
Layout Partials.
***/

/* Setup Layout Part - Header */
MetronicApp.controller('HeaderController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initHeader(); // init header
    });
}]);

/* Setup Layout Part - Sidebar */
MetronicApp.controller('SidebarController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initSidebar(); // init sidebar
    });
}]);

/* Setup Layout Part - Sidebar */
MetronicApp.controller('PageHeadController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {        
        Demo.init(); // init theme panel
    });
}]);

/* Setup Layout Part - Footer */
MetronicApp.controller('FooterController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initFooter(); // init footer
    });
}]);

/* Setup Rounting For All Pages */
MetronicApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    // Redirect any unmatched url
    $urlRouterProvider.otherwise("/dashboard.html");

    var authenticated = ['$q', '$window','Auth', function ($q, $window, Auth) {
        var deferred = $q.defer();
        Auth.isLogged(function(data){
             deferred.resolve();
         },function(err, cod){
            deferred.reject('Not logged in');
         })
        return deferred.promise;
      }];

    $stateProvider

        // Dashboard
        .state('dashboard', {
            url: "/dashboard.html",
            templateUrl: "views/dashboard.html",            
            data: {pageTitle: 'Dashboard', pageSubTitle: 'statistics & reports'},
            controller: "DashboardController",
            resolve: {
                authenticated: authenticated,
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'theme/global/plugins/morris/morris.css',
                            'theme/admin/pages/css/tasks.css',
                            
                            'theme/global/plugins/morris/morris.min.js',
                            'theme/global/plugins/morris/raphael-min.js',
                            'theme/global/plugins/jquery.sparkline.min.js',

                            'theme/admin/pages/scripts/index3.js',
                            'theme/admin/pages/scripts/tasks.js',

                             'js/controllers/DashboardController.js'
                        ] 
                    });
                }]
            }
        })

        // Users List
        .state('users', {
            url: "/users.html",
            templateUrl: "views/users/list.html",
            data: {pageTitle: 'Usuários', pageSubTitle: 'Listagem de Usúarios'},
            controller: "UsersList",
            resolve: {
                authenticated: authenticated,
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'js/services/TownhallsService.js',
                            'js/services/UsersService.js',
                            'js/controllers/UsersController.js'
                        ]
                    });
                }]
            }
        })

        // Users Create
        .state('users_create', {
            url: "/users/create.html",
            templateUrl: "views/users/create.html",
            data: {pageTitle: 'Usuários', pageSubTitle: 'Criar Usuário'},
            controller: "UserCreate",
            resolve: {
                authenticated: authenticated,
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'js/services/TownhallsService.js',
                            'js/services/UsersService.js',
                            'js/controllers/UsersController.js'
                        ]
                    });
                }]
            }
        })

        // Users Edit
        .state('users_edit', {
            url: "/users/edit.html/:id",
            templateUrl: "views/users/create.html",
            data: {pageTitle: 'Usuários', pageSubTitle: 'Editar Usuário'},
            controller: "UserUpdate",
            resolve: {
                authenticated: authenticated,
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'js/services/TownhallsService.js',
                            'js/services/UsersService.js',
                            'js/controllers/UsersController.js'
                        ]
                    });
                }]
            }
        })

        // Townhall List
        .state('townhall', {
            url: "/townhalls.html",
            templateUrl: "views/townhalls/list.html",
            data: {pageTitle: 'Prefeituras', pageSubTitle: 'Lista de Prefeituras'},
            controller: "TownhallList",
            resolve: {
                authenticated: authenticated,
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'js/services/TownhallsService.js',
                            'js/controllers/TownhallsController.js'
                        ]
                    });
                }]
            }
        })

        // Users Create
        .state('townhall_create', {
            url: "/townhalls/create.html",
            templateUrl: "views/townhalls/create.html",
            data: {pageTitle: 'Prefeituras', pageSubTitle: 'Criar Prefeituras'},
            controller: "TownhallCreate",
            resolve: {
                authenticated: authenticated,
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'js/services/TownhallsService.js',
                            'js/controllers/TownhallsController.js'
                        ]
                    });
                }]
            }
        })

        // Users Edit
        .state('townhall_edit', {
            url: "/townhalls/edit.html/:id",
            templateUrl: "views/townhalls/create.html",
            data: {pageTitle: 'Prefeituras', pageSubTitle: 'Alterar Prefeituras'},
            controller: "TownhallUpdate",
            resolve: {
                authenticated: authenticated,
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'js/services/TownhallsService.js',
                            'js/controllers/TownhallsController.js'
                        ]
                    });
                }]
            }
        })

        // Services List
        .state('services', {
            url: "/services.html",
            templateUrl: "views/services/list.html",
            data: {pageTitle: 'Serviços', pageSubTitle: 'Lista de Serviços'},
            controller: "ServicesList",
            resolve: {
                authenticated: authenticated,
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'js/services/ServicesService.js',
                            'js/controllers/ServicesController.js'
                        ]
                    });
                }]
            }
        })

        // Services Create
        .state('services_create', {
            url: "/services/create.html",
            templateUrl: "views/services/create.html",
            data: {pageTitle: 'Serviços', pageSubTitle: 'Cadastro de  Serviços'},
            controller: "ServicesCreate",
            resolve: {
                authenticated: authenticated,
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'js/services/ServicesService.js',
                            'js/controllers/ServicesController.js'
                        ]
                    });
                }]
            }
        })

        // Services Edit
        .state('services_edit', {
            url: "/services/edit.html/:id",
            templateUrl: "views/services/create.html",
            data: {pageTitle: 'Serviços', pageSubTitle: 'Cadastro de  Serviços'},
            controller: "ServicesUpdate",
            resolve: {
                authenticated: authenticated,
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'js/services/ServicesService.js',
                            'js/controllers/ServicesController.js'
                        ]
                    });
                }]
            }
        })

        // NotaryUses List
        .state('notaryuses', {
            url: "/notaryuses.html",
            templateUrl: "views/notaryuses/list.html",
            data: {pageTitle: 'Serventias', pageSubTitle: 'Listagem de Serventias'},
            controller: "NotaryUsesList", 
            resolve: {
                authenticated: authenticated,
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'js/services/NotaryUsesService.js', 
                            'js/controllers/NotaryUsesController.js'
                        ]
                    });
                }]
            }
        })

        // NotaryUses Create
        .state('notaryuses_create', {
            url: "/notaryuses/create.html",
            templateUrl: "views/notaryuses/create.html",
            data: {pageTitle: 'Serventias', pageSubTitle: 'Cadastro de Serventias'},
            controller: "NotaryUseCreate",
            resolve: {
                authenticated: authenticated,
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'js/services/NotaryUsesService.js',
                            'js/controllers/NotaryUsesController.js'
                        ]
                    });
                }]
            }
        })

        // NotaryUses Edit
        .state('notaryuses_edit', {
            url: "/notaryuses/edit.html/:id",
            templateUrl: "views/notaryuses/create.html",
            data: {pageTitle: 'Serventias', pageSubTitle: 'Edição de Serventias'}, 
            controller: "NotaryUseUpdate",
            resolve: {
                authenticated: authenticated,
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'js/services/NotaryUsesService.js', 
                            'js/controllers/NotaryUsesController.js'
                        ]
                    });
                }]
            }
        })

        // Notary List
        .state('notaries', {
            url: "/notaries.html",
            templateUrl: "views/notaries/list.html",
            data: {pageTitle: 'Cartórios', pageSubTitle: 'Listagem de Cartórios'},
            controller: "NotariesList", 
            resolve: {
                authenticated: authenticated,
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'js/services/NotariesService.js', 
                            'js/controllers/NotariesController.js'
                        ]
                    });
                }]
            }
        })

        // Notary Create
        .state('notary_create', {
            url: "/notaries/create.html",
            templateUrl: "views/notaries/create.html",
            data: {pageTitle: 'Cartórios', pageSubTitle: 'Cadastro de Cartórios'},
            controller: "NotaryCreate",
            resolve: {
                authenticated: authenticated,
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'js/services/NotaryUsesService.js', 
                            'js/services/ServicesService.js', 
                            'js/services/NotariesService.js', 
                            'js/controllers/NotariesController.js'
                        ]
                    });
                }]
            }
        })

        // Notary Edit
        .state('notary_edit', {
            url: "/notaries/edit.html/:id",
            templateUrl: "views/notaries/create.html",
            data: {pageTitle: 'Cartórios', pageSubTitle: 'Edição de Cartórios'}, 
            controller: "NotaryUpdate",
            resolve: {
                authenticated: authenticated,
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'js/services/NotaryUsesService.js', 
                            'js/services/ServicesService.js',
                            'js/services/NotariesService.js', 
                            'js/controllers/NotariesController.js'
                        ]
                    });
                }]
            }
        })

        // Logout
        .state('logout', {
            url: "/logout.html",
            //templateUrl: "views/services/create.html",
            data: {pageTitle: 'Logout', pageSubTitle: 'Logout'},
            controller: "LogoutController",
            resolve: {
                authenticated: authenticated,
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'js/services/LoginService.js',
                            'js/controllers/LoginController.js'
                        ]
                    });
                }]
            }
        })

        // Sales
        .state('sales', {
            url: "/sales.html",
            templateUrl: "views/sales/list.html",
            data: {pageTitle: 'Vendas', pageSubTitle: 'Vendas'},
            controller: "SalesList",
            resolve: {
                authenticated: authenticated,
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'js/services/ServicesService.js',
                            'js/services/SalesService.js',
                            'js/controllers/SalesController.js'
                        ]
                    });
                }]
            }
        })

        // Sales
        .state('sales_create', {
            url: "/sales/create.html",
            templateUrl: "views/sales/create.html",
            data: {pageTitle: 'Nova Venda', pageSubTitle: 'Vendas'},
            controller: "SalesCreate",
            resolve: {
                authenticated: authenticated,
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'js/services/ServicesService.js',
                            'js/services/SalesService.js',
                            'js/controllers/SalesController.js'
                        ]
                    });
                }]
            }
        })


}]);

/* Init global settings and run the app */
MetronicApp.run(["$rootScope", "settings", "$state", function($rootScope, settings, $state) {
    $rootScope.$state = $state;
}]);

MetronicApp.run(function ($rootScope, $state, $log, $window) {
  $rootScope.$on('$stateChangeError', function () {
    $window.location = '../index.html';
  });
});

