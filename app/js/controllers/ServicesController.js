(function(){   

	'use strict';

	MetronicApp.controller('ServicesList', ['$rootScope', '$scope', '$location', 'Services', function($rootScope, $scope, $location, Services){

	    $scope.$on('$viewContentLoaded', function() {   
	    	Metronic.initAjax();
	    });

	    Services.getAll(null, function(data){
	    	$scope.services = data;
	    });

	    $scope.save = function(service){
	    	Services.create(service, function(data){
	    		$location.path('/services.html');
	    	})
	    }

	    $scope.modal = function(service) {
	    	$scope.servicem = service;
	    }

	    $scope.remove = function(service) {
	    	Services.delete(service.id_servico, function(){
	    		var index = $scope.services.indexOf(service);
				$scope.services.splice(index, 1);
	    	})
	    }


	}]);

	MetronicApp.controller('ServicesCreate', ['$rootScope', '$scope', '$location', 'Services', function($rootScope, $scope, $location, Services){

	    $scope.$on('$viewContentLoaded', function() {   
	    	Metronic.initAjax();
	    });

	    $scope.save = function(service){
	    	Services.create(service, function(data){
	    		$location.path('/services.html');
	    	})
	    }

	}]);

	MetronicApp.controller('ServicesUpdate', ['$rootScope', '$scope', '$location', '$stateParams', 'Services', function($rootScope, $scope, $location, $stateParams, Services){

	    $scope.$on('$viewContentLoaded', function() {   
	    	Metronic.initAjax();
	    });

		Services.getById($stateParams.id, function(data){
	    	$scope.service = data;
	    });

	    $scope.save = function(service){
	    	Services.update(service.id_servico, service, function(data){
	    		$location.path('/services.html');
	    	})
	    }

	}]);


}());
