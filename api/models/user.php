<?php

class User extends \Illuminate\Database\Eloquent\Model {
	protected $table = 'usuario';
	protected $primaryKey = 'id_usuario';
	public $timestamps = false;
}

class UserHasTowHall extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'usuario_has_prefeitura';
    protected $primaryKey = 'id_usuario';
    public $timestamps = false;
}
