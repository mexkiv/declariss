<?php

class Notary extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'cartorio';
    protected $primaryKey = 'id_cartorio';
    public $timestamps = false;
}

class NotaryHasNotaryUse extends \Illuminate\Database\Eloquent\Model {
	protected $table = 'cartorio_has_cartorio_serventia';
    protected $primaryKey = 'id_cartorio';
    public $timestamps = false;
}

class NotaryHasService extends \Illuminate\Database\Eloquent\Model {
	protected $table = 'cartorio_has_servico';
    protected $primaryKey = 'id_cartorio';
    public $timestamps = false;
}
