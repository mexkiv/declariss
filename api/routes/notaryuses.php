<?php

require_once 'controllers/notaryuses.php';
require_once 'controllers/auth.php';

// GET Route

$app->get('/notaryuses', function () use ($app) {
	\Auth\isAuthenticate();
	\NotaryUses\retrieve($app);
});

// GET Route

$app->get('/notaryuses/:id', function ($id) use ($app) {
	\Auth\isAuthenticate();
	\NotaryUses\retrieve($app, $id);
});

// POST Route

$app->post('/notaryuses', function () use ($app) {
	\Auth\isAuthenticate();
	\NotaryUses\create($app);
});

// PUT Route

$app->put('/notaryuses/:id', function ($id) use ($app) {
	\Auth\isAuthenticate();
	\NotaryUses\update($app, $id);
});

// DELETE Route

$app->delete('/notaryuses/:id/', function ($id) use ($app) {
	\Auth\isAuthenticate();
	\NotaryUses\remove($app, $id);
});
