(function(){    

    'use strict';

    MetronicApp.factory('Sales', ['$http', 'Utils', function($http, Utils) {
        
        var sale = {};
        var api = '../api/sales';

        sale.getAll =  function(id, callback) {
            return $http.get(api).success(callback);
        }

        sale.getById =  function(id, callback) {
            return $http.get(api + '/' + id).success(callback);
        }

        sale.create = function(data, callback) {
        	return $http.post(api, data).success(callback);
        }

        sale.update = function(id, data, callback) {
        	return $http.put(api + '/' + id, data).success(callback);
        }

        sale.delete = function(id, callback) {
        	return $http.delete(api + '/' + id).success(callback);
        }


        return sale;

    }]);

}());