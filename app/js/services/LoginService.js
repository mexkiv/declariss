(function(){    

    'use strict';

    MetronicApp.factory('Auth', ['$http', function($http) {
        
        var auth = {};

        auth.isLogged =  function(callback1, callback2) {
            return $http.get('../api/auth').success(callback1).error(callback2);
        }

        auth.doLogin = function(data, callback1, callback2) {
        	return $http.post('api/auth', data).success(callback1).error(callback2);
        }

        auth.doLogout = function(callback) {
            return $http.get('../api/logout').success(callback);
        }

        return auth;

    }]);

}());