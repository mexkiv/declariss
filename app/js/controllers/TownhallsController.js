(function(){   

	'use strict';

	MetronicApp.controller('TownhallList', ['$rootScope', '$scope', '$location', 'Townhalls', function($rootScope, $scope, $location, Townhalls){

	    $scope.$on('$viewContentLoaded', function() {   
	    	Metronic.initAjax();
	    });

	    Townhalls.getAll(null, function(data){
	    	$scope.townhalls = data;
	    });

	    $scope.save = function(townhall){
	    	Townhalls.create(townhall, function(data){
	    		$location.path('/townhalls.html');
	    	})
	    }

	    $scope.modal = function(townhall) {
	    	$scope.townhallm = townhall;
	    }

	    $scope.remove = function(townhall) {
	    	Townhalls.delete(townhall.id_prefeitura, function(){
	    		var index = $scope.townhalls.indexOf(townhall);
				$scope.townhalls.splice(index, 1);
	    	},function(){
	    		townhall.rem = true;
	    	})
	    }


	}]);


	MetronicApp.controller('TownhallCreate', ['$rootScope', '$scope', '$location', 'Townhalls', function($rootScope, $scope, $location, Townhalls){

	    $scope.$on('$viewContentLoaded', function() {   
	    	Metronic.initAjax();
	    });

	    $scope.save = function(townhall, form){
	    	townhall.cnpj = form.cnpj.$viewValue;
	    	Townhalls.create(townhall, function(data){
	    		$location.path('/townhalls.html');
	    	})
	    }

	}]);



	MetronicApp.controller('TownhallUpdate', ['$rootScope', '$scope', '$location', '$stateParams', 'Townhalls', function($rootScope, $scope, $location, $stateParams, Townhalls){

	    $scope.$on('$viewContentLoaded', function() {   
	    	Metronic.initAjax();
	    });

		Townhalls.getById($stateParams.id, function(data){
	    	$scope.townhall = data;
	    });

	    $scope.save = function(townhall, form){
	    	townhall.cnpj = form.cnpj.$viewValue;
	    	Townhalls.update(townhall.id_prefeitura, townhall, function(data){
	    		$location.path('/townhalls.html');
	    	})
	    }

	}]);



}());
