<?php

require_once 'controllers/notarys.php';
require_once 'controllers/auth.php';

// GET Route

$app->get('/notarys', function () use ($app) {
	\Auth\isAuthenticate();
	\Notarys\retrieve($app);
});

// GET Route

$app->get('/notarys/:id', function ($id) use ($app) {
	\Auth\isAuthenticate();
	\Notarys\retrieve($app, $id);
});

// GET notaryuses Route

$app->get('/notarys/notaryuses/:id', function ($id) use ($app) {
	\Auth\isAuthenticate();
	\Notarys\notaryuses($app, $id);
});

// GET notaryservices Route

$app->get('/notarys/notaryservices/:id', function ($id) use ($app) {
	\Auth\isAuthenticate();
	\Notarys\notaryservices($app, $id);
});

// POST Route

$app->post('/notarys', function () use ($app) {
	\Auth\isAuthenticate();
	\Notarys\create($app);
});

// PUT Route

$app->put('/notarys/:id', function ($id) use ($app) {
	\Auth\isAuthenticate();
	\Notarys\update($app, $id);
});

// DELETE Route

$app->delete('/notarys/:id/', function ($id) use ($app) {
	\Auth\isAuthenticate();
	\Notarys\remove($app, $id);
});
