(function(){    

    'use strict';

    MetronicApp.factory('Utils', ['$http', function($http) {
        
        var util = {};

        util.appUrl =  function() {
            var l = window.location;
            var url = l.protocol + '//' + l.host + '/' + l.pathname.split('/')[1] + '/' + 'app/';
            return url;
        }

        util.apiUrl =  function() {
            var l = window.location;
            var url = l.protocol + '//' + l.host + '/' + l.pathname.split('/')[1] + '/' + 'api/';
            return url;
        }

        return util;

    }]);

}());